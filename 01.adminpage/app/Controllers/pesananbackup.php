<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Pesanan_model;

class Pesanan extends Controller

{
    protected $mpesanan;

    public function __construct()
    {

        $this->mpesanan =  new Pesanan_model();
    }

    public function index()
    {
        $getdata = $this->mpesanan->getdata();

        $data = array(
            'dataPesanan' => $getdata,
        );

        var_dump($getdata);
        return view('admin', $data);
    }
}
