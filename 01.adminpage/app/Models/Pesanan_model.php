<?php

namespace App\Models;

use CodeIgniter\Model;

class Pesanan_model extends Model
{
    public function getdata()
    {
        $query = $this->db->query("SELECT *FROM pesanan");

        return $query->getResult();
    }
}
