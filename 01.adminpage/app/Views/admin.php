<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>AdminToast</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
	<!-- Morris chart -->
	<link rel="stylesheet" href="/bower_components/morris.js/morris.css">
	<!-- jvectormap -->
	<link rel="stylesheet" href="/bower_components/jvectormap/jquery-jvectormap.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-light">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="index2.html" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>A</b>T</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Admin</b>Toast</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<li class="dropdown messages-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-envelope-o"></i>
								<span class="label label-success"></span>
							</a>
							<ul class="dropdown-menu">

								<li>
									<!-- inner menu: contains the actual data -->
									<ul class="menu">

										<!-- end message -->



									</ul>
								</li>
								<li class="footer"><a href="#">See All Messages</a></li>
							</ul>
						</li>
						<!-- Notifications: style can be found in dropdown.less -->
						<li class="dropdown notifications-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-bell-o"></i>
								<span class="label label-warning"></span>
							</a>
							<ul class="dropdown-menu">

								<li>
									<!-- inner menu: contains the actual data -->
									<ul class="menu">

									</ul>
								</li>
								<li class="footer"><a href="#">View all</a></li>
							</ul>
						</li>
						<!-- Tasks: style can be found in dropdown.less -->

						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="/dist/img/ToastTeam Logo.png" class="user-image" alt="User Image">
								<span class="hidden-xs">Admin Toast</span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-right">
								<a href="/logout" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul>
					</li>
					<!-- Control Sidebar Toggle Button -->

					</ul>
				</div>
			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="/dist/img/ToastTeam Logo.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Admin Toast</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- search form -->
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu" data-widget="tree">
					<li class="active treeview">
						<a href="<?php echo base_url('/'); ?>">
							<i class="fa fa-home"></i> <span>Beranda</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="active"><a href="<?php echo base_url('/Pesanan'); ?>"><i class="fa fa-book"></i>Pesanan</a></li>
						</ul>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Pesanan
					<small>Data Pesanan</small>
				</h1>

			</section>
			<!-- Main content -->
			<section class="content">

				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">Tabel Pesanan</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<table class="table table-bordered">

									<tr>
										<th style="width: 10px">No</th>
										<th>Nama_user</th>
										<th>No_hp</th>
										<th>Kategori</th>
										<th>Nama_produk</th>
										<th>Jumlah</th>
										<th>Alamat</th>
										<th>catatan</th>
									</tr>
									<?php $no = 1;
									foreach ($dataPesanan as $row) : ?>
										<tr>
											<td><?= $no++; ?>

											<td><?= $row->nama_user; ?></td>
											<td><?= $row->no_hp; ?></td>
											<td><?= $row->kategori; ?></td>
											<td><?= $row->nama_produk; ?></td>
											<td><?= $row->jumlah; ?></td>
											<td><?= $row->alamat; ?></td>
											<td><?= $row->catatan; ?></td>

										<?php endforeach ?>

										<!--	<td>1.</td>

										<td>Madhani</td>
										<td>085748384683</td>
										<td>Snack</td>
										<td>Paket1</td>
										<td>10</td>
										<td>Kampus 4 Universitas Ahmad Dahlan</td>
										<td>Terima kasih</td>

										</tr>
										<tr>
											<td>2.</td>

											<td>Hahan</td>
											<td>089866686812</td>
											<td>Snack</td>
											<td>Paket 3</td>
											<td>30</td>
											<td>Kampus 4 Universitas Ahmad Dahlan</td>
											<td>Terima kasih</td>
										</tr>
										<tr>
											<td>3.</td>

											<td>wulan</td>
											<td>081345678821</td>
											<td>Tumpeng</td>
											<td>Paket 5</td>
											<td>1</td>
											<td>Taman Siswa Business Centre, Jl. Taman Siswa No.160, Wirogunan, Kec. Mergangsan, Kota Yogyakarta, Daerah Istimewa Yogyakarta</td>
											<td>Terima kasih</td>

										</tr>
										<tr>
											<td>4.</td>

											<td>Firos</td>
											<td>081234895609</td>
											<td>Salad</td>
											<td>Peket 2</td>
											<td>12</td>
											<td>Kampus 5 Universitas Ahmad Dahlan</td>
											<td>Terima kasih</td>

										</tr>
										<tr>
											<td>5.</td>

											<td>akbar</td>
											<td>081234567865</td>
											<td>Nasi Kotak</td>
											<td>Paket2</td>
											<td>15</td>
											<td>Kampus 4 Universitas Ahmad Dahlan</td>
											<td>Terima kasih</td>

										</tr>
										<tr>
											<td>6.</td>

											<td>habib</td>
											<td>081380983746</td>
											<td>Nasi Kotak</td>
											<td>Paket 1</td>
											<td>20</td>
											<td> Grojogan, Tamanan, Kec. Banguntapan, Kabupaten Bantul, Daerah Istimewa Yogyakarta</td>
											<td>Terima kasih</td>

										</tr>
										<tr>
											<td>7.</td>

											<td>nanda</td>
											<td>081276589403</td>
											<td>Snack </td>
											<td>Paket 6</td>
											<td>150</td>
											<td>Kampus 4 Universitas Ahmad Dahlan</td>
											<td>Terima kasih</td>

										</tr>
										<tr>
											<td>8.</td>

											<td>Riki</td>
											<td>098764536278</td>
											<td>Nasi Kotak</td>
											<td>Paket 3</td>
											<td>30</td>
											<td>Jl. Garuda No.41, RT.16/RW.05, Sorosutan, Kec. Umbulharjo, Kota Yogyakarta, Daerah Istimewa Yogyakarta</td>
											<td>Terima kasih</td>

										</tr>
										<tr>
											<td>9.</td>

											<td>Ersa</td>
											<td>082345784366</td>
											<td>Tumpeng</td>
											<td>Paket 6</td>
											<td>1</td>
											<td>Kampus 4 Universitas Ahmad Dahlan</td>
											<td>Terima kasih</td>

										</tr> -->


								</table>
							</div>
							<!-- /.box-body -->
							<div class="box-footer clearfix">
								<ul class="pagination pagination-sm no-margin pull-right">
									<li><a href="#">&laquo;</a></li>
									<li><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">&raquo;</a></li>
								</ul>
							</div>
						</div>
						<!-- /.box -->

			</section>
			<!-- /.content -->




			<!-- ./wrapper -->

			<!-- jQuery 3 -->
			<script src="bower_components/jquery/dist/jquery.min.js"></script>
			<!-- jQuery UI 1.11.4 -->
			<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
			<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
			<script>
				$.widget.bridge('uibutton', $.ui.button);
			</script>
			<!-- Bootstrap 3.3.7 -->
			<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
			<!-- Morris.js charts -->
			<script src="bower_components/raphael/raphael.min.js"></script>
			<script src="bower_components/morris.js/morris.min.js"></script>
			<!-- Sparkline -->
			<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
			<!-- jvectormap -->
			<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
			<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
			<!-- jQuery Knob Chart -->
			<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
			<!-- daterangepicker -->
			<script src="bower_components/moment/min/moment.min.js"></script>
			<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
			<!-- datepicker -->
			<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
			<!-- Bootstrap WYSIHTML5 -->
			<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
			<!-- Slimscroll -->
			<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
			<!-- FastClick -->
			<script src="bower_components/fastclick/lib/fastclick.js"></script>
			<!-- AdminLTE App -->
			<script src="dist/js/adminlte.min.js"></script>
			<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
			<script src="dist/js/pages/dashboard.js"></script>
			<!-- AdminLTE for demo purposes -->
			<script src="dist/js/demo.js"></script>
</body>

</html>